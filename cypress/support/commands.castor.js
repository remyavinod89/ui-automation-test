let email = Cypress.env("email")
let password = Cypress.env("password")
let progressBar = '.data-entry-progress-container'

Cypress.Commands.add('loginCastor', function () {
  cy.get('#field-username').type(email)
  cy.get('#field-password').type(password)
  cy.get('.Buttons > .Button > span').click()
});

Cypress.Commands.add('addRecord', function () {
  cy.get('.StudyTitle > span').click()
  cy.get('#create_record_button > .StyledContent-bh1z1h-0').click()
  cy.get('.sc-fzoyAV > .UxzOM > .StyledContent-bh1z1h-0').click()    });

Cypress.Commands.add('enterDetails', function () {
  cy.get('#0DE587C6-3053-45C8-B8A8-3987620827E4-0').check().should('be.checked')
  cy.get('#ext-comp-1620items > :nth-child(1)').check().should('be.checked')
  cy.get('ext-comp-2020').type('1')
  cy.get('#55C21D26-863C-40EA-8506-20910DC84A49-0').check().should('be.checked')
  cy.get('ext-comp-1766').click()
  
  cy.get('#830BBE0E-F479-4831-8E1A-7256DD576A39-0').check().should('be.checked')
  cy.get('#99FA9164-E322-4AFC-8F74-E69674751C15-0').check().should('be.checked')
  cy.get('#7BE63AA9-D377-4CDC-8403-94FE7C55A1BF-0').check().should('be.checked')
  cy.get('#CFC6A0EE-7E48-4378-8DC3-6D26C0BC7EB9-0').check().should('be.checked')
  cy.get('#ext-gen414').click()

  cy.get('#ext-comp-2726').type('1989')
  cy.get('#ACEE3186-4160-4305-89D4-BF418F436543-0').check().should('be.checked')
  cy.get('#css-lt9a2').select('Asian').should('have.value', 'Asian')
  cy.get('#ext-comp-2731').type('2')
  cy.get('#ext-gen414').click()

  cy.get('#2D89285B-4CCB-497A-B099-228172766611-0').click()
  cy.get('#ADCEF761-F165-405A-8E81-A83AA0B090B1-0').click()
  cy.get('#CAAE009B-9CDA-41F7-84AB-84FBC1568FD3-0').click()
  cy.get('#E2D6C436-6DE8-41BC-AB46-7A199052844C-0').check().should('be.checked')
  cy.get('#ext-gen414').click()
  cy.get('#ext-gen414').click()

  cy.get('#ext-comp-3340').type('60')
  cy.get('#ext-comp-3345').type('65')
  cy.get('#ext-comp-3348').type('67')
  cy.get('#ext-comp-3351').type('67')
  cy.get('#ext-gen414').click()
});

Cypress.Commands.add('checkCompletedRecors', function () {
  if (cy.get(progressBar).should('contain', '100%')) {
    console.log('The record is completed')
  }
  else {
    console.log('The record is not completed')
  }
});

Cypress.Commands.add('clearData', function () {
  cy.get('#ext-comp-8173').type(67)
  cy.get('#ext-comp-8114menu-image').click()
  cy.get('#ext-gen2227').click()
  cy.get('ext-comp-8173').should('be.empty')
  console.log | ('validated textbox is empty')
});

Cypress.Commands.add('validateClearData', function () {
  cy.get('ext-comp-8173').should('be.empty')
  console.log | ('validated textbox is empty')
});
















