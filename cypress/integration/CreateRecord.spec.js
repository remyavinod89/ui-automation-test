describe('Create a complete record and validate whether its completed', () => {
    before('Given the user logs in', () => {
        cy.loginCastor();
    })
    it('And navigates to the create record', () => {
        cy.addRecord();
    })
    it('And user enter the details', () => {
        cy.enterDetails();
    })
    it('Then record should be created and completed', () => {
        checkIncompletedRecord();
    });
});

describe('Create an incomplete record and validate whether its incompleted', () => {
    before('Given the user logs in', () => {
        cy.loginCastor();
    })
    it('And navigates to the create record', () => {
        cy.addRecord();
    })
    it('And user enter the details', () => {
        cy.enterDetails();
    })
    it('Then record should be created and incompleted', () => {
        cy.checkCompletedRecors();
    });
});