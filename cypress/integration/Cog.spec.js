before(function () {
    cy.loginCastor();
})

describe('Actions with cog elements', () => {
    describe('Clear action', () => {
        it('Given user enter the data and the user clear the data', () => {
            cy.clearData();
        })

        it('Then validate the data should be cleared', () => {
            cy.validateClearData()
        });
    });

    describe('comments on cog', () => {
        it('Given user enter the data', () => {

        })
        it('And user post a comment', () => {

        })
        it('Then validate the post comment should be there', () => {

        });
    });
});